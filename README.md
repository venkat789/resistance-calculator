This project is to calculate resistance by passing color codes
First Band
Second Band
Multiplier Band
Tolerance Band

Resistance will be calculated based on the first 3 values and it's min and max range are adjusted by tolerance.

How to run this app:
Run below commands in command prompt from root of this folder
1) npm install
2) npm start  (To run the project- localhost:3000 )
3) npm test (it will run unit test cases)