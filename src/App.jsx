import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import AppHeader from './Header'
import ResistorBands from './ResistorBands';
import ErrorBoundary from './ErrorBoundry'
class App extends Component {

  render() {
    return (
      <ErrorBoundary>
        <div className="container">
          <AppHeader logo={logo} />
          <ResistorBands />
        </div>
      </ErrorBoundary>   
    );
  }
}
 
export default App;
