import React from 'react'
import { invalidParametersError } from './error';

const Select = (props) => {

    let areInputValuesCorrect = props.labelName && props.options && (typeof props.change === 'function')
    
    if (!areInputValuesCorrect) {
        throw invalidParametersError
        return
    }
    this.onChangeHandler = (event) => {
        props.change(event, props.target)
    }

    return (
        <div >
            <label className="col-sm-4 control-label"> {props.labelName} </label>
            <div className="col-sm-8">
                <select className="form-control" onChange={this.onChangeHandler} value={props.value}>
                    {props.options.map(option =>
                        <option key={option.key} value={option.value}>
                            {option.label}
                        </option>)}  
                </select>
            </div>
        </div>
    )
}
export default Select 