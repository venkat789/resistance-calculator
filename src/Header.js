import React  from 'react';

export default function AppHeader(props){
    return (
        <header className="App-header">
            <img src={props.logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Resistance Calculator</h1>
        </header>
    )
}
