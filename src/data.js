export default {
    bandValues: [
      
        { key: 1, label: "Brown (1)", value: "1" },
        { key: 2, label: "Red (2)", value: "2" },
        { key: 3, label: "Orange (3)", value: "3" },
        { key: 4, label: "Yellow (4)", value: "4" },
        { key: 5, label: "Green (5)", value: "5" },
        { key: 6, label: "Blue (6)", value: "6" },
        { key: 7, label: "Violet (7)", value: "7" },
        { key: 8, label: "Gray (8)", value: "8" },
        { key: 9, label: "White (9)", value: "9" }],
    toleranceValues: [
        {key: 0, label: "Brown (\u00B1 1%)", value: "1"},
        {key: 1, label: "Red (\u00B1 2%)", value: "2"},
        {key: 2, label: "Green (\u00B1 0.5%)", value: "0.5"},
        {key: 3, label: "Blue (\u00B1 0.25%)", value: "0.25"},
        { key: 4, label: "Violet (\u00B1 0.1%)", value: "0.1" },
        { key: 5, label: "Gray (\u00B1 0.05%)", value: "0.05" },
        { key: 6, label: "Gold (\u00B1 5%)", value: "5" },
        { key: 7, label: "Silver (\u00B1 10%)", value: "10" }
    ],
    multiplierValues: [
        { key: 0, label: "Black (1)", value: "1" },
        { key: 1, label: "Brown (10)", value: "10" },
        { key: 2, label: "Red (100)", value: "100" },
        { key: 3, label: "Orange (1K)", value: "1000" },
        { key: 4, label: "Yellow (10K)", value: "10000" },
        { key: 5, label: "Green (100K)", value: "100000" },
        { key: 6, label: "Blue (1M)", value: "1000000" },
        { key: 7, label: "Violet (10M)", value: "10000000" },
        { key: 8, label: "Gray (100M)", value: "100000000" },
        { key: 9, label: "White (1B)", value: "1000000000" },
        { key: 10, label: "Pink (0.001)", value: "0.001" },
        { key: 11, label: "Silver (0.01)", value: "0.01" },
        { key: 12, label: "Gold (0.1)", value: "0.1" }
    ],

   
}