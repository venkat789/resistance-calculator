import { invalidParametersError } from './error';

export default function calculateOhmValue(bandColors) {
  if (typeof bandColors === 'undefined'  || bandColors == null )     
  throw invalidParametersError

  const {
    firstNumber,
    secondNumber, 
    multiplierNumber, 
    toleranceNumber
  } = bandColors;
  console.log(bandColors) 

  let areInputValuesCorrect = firstNumber && secondNumber && multiplierNumber
  if(!areInputValuesCorrect)
  throw invalidParametersError

  const baseOhmValue = (
    ((firstNumber * 10) + secondNumber) * multiplierNumber
  );
  const toleranceOffset = baseOhmValue * (toleranceNumber / 100);
  const minimumOhmValue = baseOhmValue - toleranceOffset;
  const maximumOhmValue = baseOhmValue + toleranceOffset;
  
  return {
    minimumOhmValue,
    maximumOhmValue,
  };

}
