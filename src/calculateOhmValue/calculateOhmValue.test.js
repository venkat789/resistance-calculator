import calculateOhmValue from './calculateOhmValue'
import { invalidParametersError } from './error';


describe('calculateOhmValue', () => {
  describe('given valid band values', () => {
    it('returns accurate maximum and minimum ohms', () => {
      const bandColors= {
        firstNumber:1, //Brown
        secondNumber:1, //Brown
        multiplierNumber:1000, //Orange
        toleranceNumber:1 //Brown
      } 

      const { maximumOhmValue, minimumOhmValue } = calculateOhmValue(bandColors);

      expect(maximumOhmValue).toBe(11110);
      expect(minimumOhmValue).toBe(10890);
    });
  });
  
  describe('given invalid band colors throw exception',()=>{
    const bandColors= {
      firstNumber:0, //Brown
      secondNumber:1, //Brown
      multiplierNumber:1000, //Orange
      toleranceNumber:1 //Brown
    } 
    expect(() => calculateOhmValue()).toThrow(invalidParametersError);
  })

});