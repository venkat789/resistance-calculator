export const invalidParametersError = `
  calculateOhmValue expects parameters with the following signature:
  firstNumber
  secondNumber
  multipleNumber
  toleranceNumber
`;