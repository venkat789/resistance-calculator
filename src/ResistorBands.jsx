import React, { Component } from 'react';
import Select from './Select/Select'
import Data from './data'
import calculateOhmValue from './calculateOhmValue/calculateOhmValue'
import './Resistorbands.css'

class ResistorBands extends Component {
    constructor() {
        super()  
        this.bandValues = Data.bandValues
        this.toleranceValues = Data.toleranceValues
        this.multiplierValues = Data.multiplierValues
        this.state = {
            firstNumber: 1,
            secondNumber: 1,
            multiplierNumber: 1,
            toleranceNumber: 1           
        }

    }
    changeHandler = (event, targetElement) => {
        console.log(event.target, targetElement)
        let selectedVal = event.target.value
        this.setState({ [targetElement]: selectedVal }, () => {
            console.log(this.state[targetElement])
        })

    };
    getResult = (state) => {
        const shouldCalculate = state.firstNumber && state.secondNumber && state.multiplierNumber;
        if (!shouldCalculate) return null;
        let ohmValues = calculateOhmValue(state)
        const ResultDiv = ({ minimumOhmValue, maximumOhmValue }) => {
            return (
                <div>
                    <div className="col-sm-4" ></div>
                    <div className="col-sm-8" >
                        <div className="info-min">
                            <h4>Minimum Ohm Value: {minimumOhmValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} Ω</h4>
                        </div>
                        <div className="info-max">
                            <h4>Maximum Ohm Value: {maximumOhmValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} Ω</h4>
                        </div>
                    </div>
                    <div className="col-sm-3" ></div>
                </div>
            )
        };
        return <ResultDiv {...ohmValues} />;

    };
    render() {
        return (
            <div className="row" style={{ paddingTop: 20 }}>
                <div className="col-sm-2"></div>
                <div className="col-sm-8">
                    <div className="form-group" style={{ paddingTop: 20 }}>
                        <Select options={this.bandValues} change={this.changeHandler} target="firstNumber" labelName="First Band" />
                        <Select options={this.bandValues} change={this.changeHandler} target="secondNumber" labelName="Second Band" />
                        <Select options={this.multiplierValues} change={this.changeHandler} target="multiplierNumber" labelName="Multiplier Band" />
                        <Select options={this.toleranceValues} change={this.changeHandler} target="toleranceNumber" labelName="Tolerance Band" />
                        {this.getResult({ ...this.state })}
                    </div>
                </div>
                <div className="col-sm-2"></div>
            </div>
        )
    }
}
export default ResistorBands
